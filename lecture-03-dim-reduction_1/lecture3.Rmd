---
title: "Dimension reduction 1: Case $n > p$ "
author: "Joerg Polzehl"
date: "MMS summer school 04.09.2018"
header-includes:
   - \usepackage{amsmath}
   - \usepackage{amssymb}
output:
  beamer_presentation: default
  ioslides_presentation: default
  widescreen: true
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE, warning=FALSE, message=FALSE)
```

---

## Why dimension reduction ?

Random variables (RV) $X \in R^p$, $Y \in R^q$, $n$ joint obs. of $(X,Y)$

Classical Multivariate Analysis: $(X,Y) \sim N_{p+q}((\mu_x,\mu_y), \Sigma_{XY})$

Interest in:

* Distribution of $X$ or $(X,Y)$

    + Parameter estimation, distribution of estimates  

* Modeling the relation of $Y$ and $X$, i.e.  $E Y|X$ (Prediction).

* Interpretation of the data

Problem: 

* Identifiability / variability of estimates if $p$ is large
* Interpretability of complex models
* Dimension reduction: reduction of variance / model complexity at the cost of bias

---

## Principle Components

Introduced by Pearson (1901), generalized by Hotelling (1933). 


Observation matrix $X = \left(x_1, \dots, x_n\right)$ with $x_i \in R^p$ realizations of a p-variate RV with 
mean $\mu$ and covariance matrix $\Sigma$.

1st PC: linear combination $\gamma_1^T x$ such that $$\gamma_1 = argmax_{\gamma:\gamma^T\gamma=1}\; \gamma^T \Sigma \gamma $$ 


$\gamma_1^T x$ maximizes the variance of any linear combination of x.

k-th PC: linear combination $\gamma_k^T x$ such that $$\gamma_k = argmax_{\gamma:\gamma^T\gamma =1,\;\gamma^T\gamma_j =0 \; \forall j<k} \;\gamma^T \Sigma \gamma $$

$\gamma_k^T x$ maximizes the variance of any linear combination of x that are uncorrelated to $\gamma_j x$ for all $j<k$.

---
output:
    incremental: true
---
---

### Characteristics

> - *Variance contribution*
$$\lambda_k = var(\gamma_k^T x) = \gamma_k^T \Sigma \gamma_k$$
$$\lambda_1 \ge \lambda_2 \ge \dots \ge \lambda_p$$
> - *Spectral decomposition*
$$ \Sigma = \lambda_1 \gamma_1 \gamma_1^T + \dots + \lambda_p \gamma_p \gamma_p^T = \Gamma \Lambda \Gamma^T$$
> - *Proportion of explained variance*
$$ \psi_k = \sum_{j=1}^k \lambda_j / \sum_{j=1}^p \lambda_j = \sum_{j=1}^k \lambda_j / tr(\Sigma) $$

> - PC's are completely determined by the covariance structure.
 This relates to multivariate Gaussian distributions !!!

---
output:
    incremental: false
---
---

### Sample Principal Components

Observation matrix $X = \left(x_1, \dots, x_n\right)$ with $x_i \in R^p$

Sample covariance
$$S = \frac{1}{n} \sum_{i=1}^n (x_i - \bar{x})(x_i - \bar{x})^T, \quad \bar{x} = \frac{1}{n} \sum_{i=1}^n x_i$$
Estimates for $\lambda_k$ and $\gamma_k$ are characteristic roots $l_k$ of
$$S-\lambda I_p$$ and corresponding eigenvectors $u_k$ with $$ (S - l_k I_p) u_k=0$$   

---

### Computation of sample PC's

consider $\tilde{X}= \left(x_1 - \bar{x}, \dots, x_n-\bar{x} \right)$

Singular value decomposition of $\tilde{X}$
$$\tilde{X} = U L V^T, \quad U^TU=I_p, \; V^TV=I_p,\; L=diag(l_1 ,\dots ,l_p)$$
$$n S= \tilde{X}\tilde{X}^T = l_1^2 u_1 u_1^T + \dots + l_p^2 u_p u_p^T = U L L U^T$$
Principal Components: columns $u_i$ of rotation matrix $U$ 

PC-scores:  $z_{ik}=u_k^T x_i$  score of $i$-th observation on the $k$-th PC

Explained variances: $l_k^2/n$ for $k$-th PC

Proportion of explained variance: $\Psi_k={\sum_{j=1}^k l_j^2}/{\sum_{j=1}^p l_j^2}$

---

### An alternative view on PCA

PCA with $r$ components provides optimal rank $r$ approximation of the mean 0 observation matrix $\tilde{X}$

$$ \sum_{k=1}^r l_k u_k v_k^T = argmin_{\hat{X} \in \mathcal{M}(r)} || \tilde{X}-\hat{X}||^2_F$$
is solution of

$$ min_{(l_1, u_1, v_1)} ||\tilde{X} - l_1 u_1 v_1^T||_F^2, \; ||u_1||^2_2=1, \; ||v_1||^2_2=1, \; l_1>0 $$
$$ min_{(l_k, u_k, v_k)} ||\tilde{X} - \sum_{j=1}^k l_j u_j v_j^T||_F^2, \; ||u_k||^2_2=1, \; ||v_k||^2_2=1,\; l_k>0, $$ $$  \qquad\qquad\qquad\qquad\qquad u_k u_j^T= v_k v_j^T = 0 \;\forall j<k $$

---

### First example: Olympic decathlon data 

Package ade4: performances of 33 men's decathlon at the Olympic Games (1988)

```{r}
library(ade4)
data(olympic)
pcaolympic <- prcomp(olympic$tab)
names(pcaolympic)
```
PCA based on Covariance matrix

---

```{r,fig.cap="Scree plot / Explained variance",fig.height=3.5,fig.width=7}
par(mfrow=c(1,2),mar=c(3,3,3,.1),mgp=c(2,1,0))
plot(pcaolympic)
plot(1:10,cumsum(pcaolympic$sdev^2)/sum(pcaolympic$sdev^2),
     xlab="k",ylab="Psi")
```

---

```{r,fig.cap="first 3 PC's and scores",fig.height=6,fig.width=7,echo=FALSE}
par(mfrow=c(2,2),mar=c(3,3,3,.1),mgp=c(2,1,0))
plot(pcaolympic$rotation[,1],ylim=range(pcaolympic$rotation),type="n",ylab="PC",main="first 3 PC's")
for(k in 1:3) lines(pcaolympic$rotation[,k],col=k)
  plot(pcaolympic$x[,1],pcaolympic$x[,2],xlab="score PC1", ylab="score PC2",main="PC 1 / PC 2",type="n")
  for(i in 1:33) text(pcaolympic$x[i,1],pcaolympic$x[i,2],paste0(i))
  plot(pcaolympic$x[,1],pcaolympic$x[,3],xlab="score PC1", ylab="score PC3",main="PC 1 / PC 3",type="n")
    for(i in 1:33) text(pcaolympic$x[i,1],pcaolympic$x[i,3],paste0(i))
  plot(pcaolympic$x[,2],pcaolympic$x[,3],xlab="score PC2", ylab="score PC3",main="PC 2 / PC 3",type="n")
    for(i in 1:33) text(pcaolympic$x[i,2],pcaolympic$x[i,3],paste0(i))

```

---

### Biplot biplot(pcaolympic)

```{r,fig.cap="Biplot (joint visualization of scores and PC's)",fig.height=7.,fig.width=7.,echo=FALSE}
par(mfrow=c(1,1),mar=c(3,3,3,.1),mgp=c(2,1,0))
biplot(pcaolympic)
```

Did we do anything wrong ??

--- 

### Data visualization

```{r,fig.cap="Biplot (joint visualization of scores and PC's)",fig.height=4,fig.width=9,echo=TRUE}
boxplot(olympic$tab)
```

Variables have different scale, different units of measurement

Use standardized observations --> Correlation matrices

---

### Olympic decathlon data: PCA based on Correlation matrix


```{r,fig.cap="Scree plot / Explained variance",fig.height=2.8,fig.width=7}
par(mfrow=c(1,2),mar=c(3,3,3,.1),mgp=c(2,1,0))
pcaolympic <- prcomp(olympic$tab,scale.=TRUE)
plot(pcaolympic)
plot(1:10,cumsum(pcaolympic$sdev^2)/sum(pcaolympic$sdev^2),
     xlab="k",ylab="Psi")
```

---

```{r,fig.cap="first 3 PC's and scores",fig.height=6,fig.width=7,echo=FALSE}
par(mfrow=c(2,2),mar=c(3,3,3,.1),mgp=c(2,1,0))
plot(pcaolympic$rotation[,1],ylim=range(pcaolympic$rotation),type="n",ylab="PC",main="first 3 PC's")
for(k in 1:3) lines(pcaolympic$rotation[,k],col=k)
  plot(pcaolympic$x[,1],pcaolympic$x[,2],xlab="score PC1", ylab="score PC2",main="PC 1 / PC 2",type="n")
  for(i in 1:33) text(pcaolympic$x[i,1],pcaolympic$x[i,2],paste0(i))
  plot(pcaolympic$x[,1],pcaolympic$x[,3],xlab="score PC1", ylab="score PC3",main="PC 1 / PC 3",type="n")
    for(i in 1:33) text(pcaolympic$x[i,1],pcaolympic$x[i,3],paste0(i))
  plot(pcaolympic$x[,2],pcaolympic$x[,3],xlab="score PC2", ylab="score PC3",main="PC 2 / PC 3",type="n")
    for(i in 1:33) text(pcaolympic$x[i,2],pcaolympic$x[i,3],paste0(i))

```

---

```{r,fig.cap="Biplot (joint visualization of scores and PC's)",fig.height=4.,fig.width=7,echo=TRUE}
par(mfrow=c(1,2),mar=c(3,3,3,2),mgp=c(2,1,0))
biplot(pcaolympic)
biplot(pcaolympic,choices=c(1,3))
```

---

### What did we learn ?

- may need standardization if variables are incomparable 

- first 6 PC's explain 90 percent of variance
- see three groups of variables (running, jumping, technical)
- first PC discriminates running from technical disciplines
- second PC correponds to balance between disciplines

- PC's are not easy to interpret (except as variance maximizing directions)
  + may be improved by rotation of PC's to get block structure of $U$
  
---

### Exercise

Perform a PCA on soil data (use ?soil to show info on data)

```{r,fig.cap="Soil data",fig.height=3.5,fig.width=7}
library(rrcovHD)
data(soil)
boxplot(soil[,-(1:2)])
```

---

### PCA dimension reduction in regression

Problem: 

* assumes that that directions of high variability in design explain variability of the response

* directions of high variability may not be informative 

* covariates can have high variability, but values of response may not depend on them

* example see Joliffe (2002) Pitprop data (Strength of Corsican pine timber depending on
13 physical measurements)

---

### Example classification: Olive oil data from Tuscany, Italy

120 samples, 25 chemical compositions, 4 production areas 

```{r,fig.cap="Olive oil data",fig.height=3.5,fig.width=7}
library(rrcovHD)
data(olitos) 
boxplot(olitos[,-26], main="Boxplot of variables")
```

---

```{r,fig.cap="Olive oil data by region",fig.height=5,fig.width=7,echo=FALSE}
region <- olitos[,26] ## 26 is region
par(mfrow=c(2,2),mar=c(3,3,3,.1),mgp=c(2,1,0))
boxplot(olitos[region==1,-26], main="Region 1")
boxplot(olitos[region==2,-26], main="Region 2")
boxplot(olitos[region==3,-26], main="Region 3")
boxplot(olitos[region==4,-26], main="Region 4")
```

---

```{r}
library(MASS)
class0 <- lda(olitos[,-26],region,CV=TRUE)
risk0 <- mean(class0$class==region)

pcaoli <- prcomp(olitos[,-26],scale.=TRUE)
signif(cumsum(pcaoli$sdev^2)/sum(pcaoli$sdev^2),3)[1:8]
results1 <- numeric(25)
for(i in 1:25){
   class1 <- lda(pcaoli$x[,1:i,drop=FALSE],region,
                 CV=TRUE)
   results1[i] <- mean(class1$class==region)
}
```

---

```{r}
voli <- matrix(0,25,4)
for(i in 1:4) voli[,i] <- 
    apply(olitos[region==i,-26],2,var)
sdoli <- sqrt(voli%*%as.numeric(table(region)-1)/
             (length(region)-4))
soli <- sweep(olitos[,-26],2,as.vector(sdoli),"/") 
pcasoli <- prcomp(soli)
signif(cumsum(pcasoli$sdev^2)/sum(pcasoli$sdev^2),3)[1:8]

results2 <- numeric(25)
for(i in 1:25){
   class1 <- lda(pcasoli$x[,1:i,drop=FALSE],region,
                 CV=TRUE)
   results2[i] <- mean(class1$class==region)
}
```

---

### Selection of number of PC's by Cross-Valdation

```{r,fig.cap="Classification results over number of PC's",fig.height=5,fig.width=7,echo=FALSE}
par(mfrow=c(1,1),mar=c(3,3,3,.1),mgp=c(2,1,0))
plot(results1,ylim=c(0.7,1),type="l",xlab="# PC's",ylab="CV-risk")
lines(results2,ylim=c(0,1),col=2)
lines(c(1,25),rep(risk0,2),col=3)
legend(15,1,c("Cor","pooled Cor","All"),lty=rep(1,3),col=1:3)


```

---


### Other dimension reduction methods

* Factor Analysis

* Canonical Correlations

* Discriminant Directions

* Independent Components 

---

### Factor Analysis

Model: $x \sim N_p(\mu, \Sigma)$ (Gaussian)
$$ x = \mu + \Lambda f + \epsilon, \; E e=0,\; E \epsilon\epsilon^T = diag(\psi),$$  
common factors $f$ with $E f = 0$, $E \epsilon f^T =0$ and $E f f^T = I_m$.
$$ \Sigma = \Lambda \Lambda^T + \Psi, \quad \Psi = diag(\psi),\; \psi \in R^p,\; \Lambda \in \mathcal{M}_{p\times k} $$
Maximum Likelihood:
   $$l(\Lambda, \psi|S) = ln |\Lambda \Lambda^T + \Psi| + tr(S |\lambda \Lambda^T + \Psi|^{-1}) - ln|S| - p$$
$\Lambda$ determined up to rotations $R$: ($\Lambda \Lambda^T = \Lambda R R^T \Lambda^T$)

can be used to enforce simple structure in $\Lambda$ optimizing some function of $\Lambda R$.

common criteria: varimax (orthogonal), promax (oblique)

---

### Decathlon data example

```{r}
volympic <- cor(olympic$tab)
faolympic <- factanal(covmat=volympic, factors=3, 
                      rotation="promax")
names(faolympic)
```
---

```{r, echo=FALSE}
faolympic$loadings
```

---

### Canonical Correlations

considers the relation between 2 sets of random variables 
$$(X,Y) \sim N_{p,q}\left(\begin{pmatrix} \mu_x\\ \mu_y  \end{pmatrix}, \begin{pmatrix} \Sigma_{xx}&\Sigma_{xy}\\\Sigma_{xy}^T&\Sigma_{yy} \end{pmatrix}\right) $$
uses sample correlation matrix $$ R = \begin{pmatrix} R_{xx}&R_{xy}\\R_{xy}^T&R_{yy} \end{pmatrix}$$ to define

$$ E_x = R^{-1}_{xx} R_{xy} R^{-1}_{yy} R_{xy}^T, \quad   E_y = R^{-1}_{yy} R_{xy}^T R^{-1}_{xx} R_{xy} $$

  + canonical correlations: eigenvalues $r_1, \dots, r_s$ of either $E_1$ or $E_2$

  + maximize the correlations between linear combinations of $X$ and $Y$.

---

### Example: Olive oil data (acis versus numeric covariasd)

```{r}
   x <- olitos[,c(1,5,6)]; y <- olitos[,7:25]
   ccolitos <- cancor(x,y)
   ccolitos$cor
   ccolitos$xcoef
```

---

### Independent Component Analysis (ICA)

 + aims for a decomposition of a zero mean observation matrix $X$ into linear combinations of
   independent components $f_i$ 
   $$ X = \Lambda F, \quad \ \in \mathcal{M}_{p\times q}, \; F \in \mathcal{M}_{q\times n}, \; q \le p $$
   $\Lambda$ - Mixing matrix,  $F$ - sources
 + similar model as factor analysis
 
 + under Gaussianity: 
    + zero correlation corresponds to independence. 
    + $\Lambda$ determined up to rotations
    
  + non-Gaussian distributions:
    +    RV's $X$, $Y$ are independent if and only if $$ F_{X,Y}(x,y) = F_{X}(x) F_{Y}(y) $$
    +    alternative $ E_{X,Y} f(x) g(y) = E_X f(x) E_Y g(y)$ for any functions $f, g$
    
---

### ICA:

  Applications: 

  - cocktail party problem
  - latent variables in econometrics
  - source estimation in EEG / MEG
  - resting state fMRI (temporal and spatial pattern)
  
  Usual steps:

  - mean extraction
  - (variance reduction)
  - initial dimension reduction using PCA
  - estimation of $\Lambda$ and $F$
  
---


### ICA-Algorithms:
  + find $f$ by maximizing non-Gaussianity of $\sum_i w_i x_i$ 
  + measures for non-Gaussianity: 
    + Kurtosis: $$E (S-E S)^4 / (E (S-E S)^2)^2$$
    + differential entropy: $$-\int f(s) log(f(s)) ds,$$ with $f$ density of $S$
    + Approximations of entropy: fix-point solutions for criteria depending on suitable non-quadratic nonlinear test functions


---


### ICA Example:

```{r}
library(fastICA)
s1 <- sin(1:512/20); s2 <- tan(1:512/17)
s3 <- 1:512%%37; s4 <- rt(512,15)
S <- rbind(s1/sd(s1),s2/sd(s2),s3/sd(s3),s4/sd(s4))
L <- matrix(c(.4,-.8,.4,.3, .4,.4,-.7,-.4, 
              -.6,.8,.3,.4, .3,-.3,.5,-.5),4,4)
X <- L%*%S+rnorm(512*4,0,.1)
set.seed(1)
z <- fastICA(t(X),4,fun="logcosh")
## or use alternatively
library(ica)
z1 <- icajade(t(X),4)
```

---

```{r,fig.cap="ICA example",fig.height=6.5,fig.width=9,echo=FALSE}
par(mfrow=c(4,4),mar=c(3,3,3,.1),mgp=c(2,1,0))
plot(S[1,],type="l",main="Source 1")
plot(X[1,],type="l",main="Observed signal 1")
plot(z$S[,1],type="l",main="est. source 1 fast")
plot(z1$S[,1],type="l",main="est. source 1 jade")
plot(S[2,],type="l",main="Source 2")
plot(X[2,],type="l",main="Observed signal 2")
plot(z$S[,2],type="l",main="est. source 2 fast")
plot(z1$S[,2],type="l",main="est. source 2 jade")
plot(S[3,],type="l",main="Source 3")
plot(X[3,],type="l",main="Observed signal 3")
plot(z$S[,3],type="l",main="est. source 3 fast")
plot(z1$S[,3],type="l",main="est. source 3 jade")
plot(S[4,],type="l",main="Source 4")
plot(X[4,],type="l",main="Observed signal 4")
plot(z$S[,4],type="l",main="est. source 4 fast")
plot(z1$S[,4],type="l",main="est. source 4 jade")
```

---


## Packages

* stats
* MASS
* fastICA
* ica

data:

* ade4
* rrcovHD



---

### Literature

+ I.T. Joliffe. Principal Component Analysis. Springer 2002

+ R.A. Johnson, D.W. Wichern. Applied Multivariate Statistical Analysis. 

+ B. Everitt, T. Hothorn. An Introduction to Applied Multivariate Analysis with R. Springer `use R!' series 

+ A. Hyvaerinen, J. Karhunen, E. Oja. Independent Component Analysis. Wiley 2001.

+ T. Hastie, R. Tibshirani, J. Friedman. The Elements of Statistical Learning. Springer 2001


