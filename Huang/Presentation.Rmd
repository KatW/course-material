---
title: "Presentation Group 2"
author: "Group 2"
date: "5 September 2018"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
knitr::opts_knit$set(root.dir = "CP16_3S")
```

## Read in the data

Data structure...

```{r data}


# read all filenames recursively
filenames <- list.files("batch1", full.names = TRUE, recursive = TRUE)

# read the first curve to get the dimension of the data
curve1 <- read.table(filenames[1])

####
# Create a matrix that has a row for each file, to save the intensities
M <- matrix(nrow = length(filenames), ncol = dim(curve1)[1])

# for all files
for(i in 1:nrow(M))
{
  #read in the i-th file
  tmp <- read.table(filenames[i])

  ### here we need to do the baseline correction
  ### Jing, please help :)
  
  # save the baseline corrected intenties in the i-th row of the matrix
  M[i, ] <- tmp[,2]  
}


```

Create a `funData` object


```{r funData, echo=TRUE}
library("funData")
allSpectra <- funData(argvals = curve1[,1], # vector of  common wave numbers
                      X = M) # matrix of intensities

plot(allSpectra)
```
