\subsection{Overview}


\begin{frame}
\frametitle{\secname}
\framesubtitle{\subsecname}
	
	\textbf{Examples of functional data:} Berkeley growth study
	
	\begin{center}
	\includegraphics[height = 0.7 \textheight]{figs/motivateData_growth.pdf}

	\end{center}
	
\end{frame}

\begin{frame}
\frametitle{\secname}
\framesubtitle{\subsecname}
	
	\textbf{Examples of functional data:} Handwriting
	
	\begin{center}
	\includegraphics[height = 0.7 \textheight]{figs/motivateData_hand.pdf}
	\end{center}
	
\end{frame}


\begin{frame}
\frametitle{\secname}
\framesubtitle{\subsecname}
	
	\textbf{Examples of functional data:} Brain scan images
	
	\begin{center}
	\includegraphics[height = 0.7 \textheight]{figs/motivateData_PET.png}
	\end{center}
	
\end{frame}

\frame{
\frametitle{\secname}
\framesubtitle{\subsecname}
	
	\textbf{Characteristics of functional data:}

	\begin{center}
	\includegraphics[height = 0.25 \textheight]{figs/motivateData_growth.pdf}
	\hfill
	\includegraphics[height = 0.25 \textheight]{figs/motivateData_hand.pdf}
	\hfill
	\includegraphics[height = 0.25 \textheight]{figs/motivateData_PET.png}
	\hfill
	\end{center}
	\pause
	
	\begin{itemize}
	\item Several measurements for the same statistical unit, often over time
		\pause
	\item Sampling grid is not necessarily equally spaced, sparse data 
		\pause
	\item Smooth variation, that could be assessed (in principle) as often as desired
		\pause
	\item Noisy observations
		\pause
	\item Many observations of the same data generating process\\
	$\leftrightarrow$ time series analysis
	\end{itemize}
	
	\hfill   {\footnotesize \cite{RamsaySilverman:2005}}
 }

\frame{
\frametitle{\secname}
\framesubtitle{\subsecname}
	
	\textbf{Aims of functional data analysis:}
	%\small 
	\begin{itemize}
	% \textcolor{lmuGruen}{}
	\item  Represent the data $\to$ interpolation, {\color<6>{lmuGruen}{smoothing}}
	\item<2-> Display the data $\to$ registration, outlier detection
	\item<3-> Study  sources of pattern and variation $\to$ {\color<6>{lmuGruen}{functional principal component analysis}}, canoncial correlation analysis
	\item<4-> Explain variation in a dependent variable by using independent variable information $\to${\color<6>{lmuGruen}{ functional regression models}}
	\item<5-> No forecasting / extrapolation $\leftrightarrow$ time series analysis
	\end{itemize}
	
	\begin{overlayarea}{\textwidth}{0.24 \textheight}
	\begin{center}
		\only<1>{
		\includegraphics[height = 0.24 \textheight]{figs/motivateDisplay_points.pdf}
		\hspace{1cm}
		\includegraphics[height = 0.24 \textheight]{figs/motivateDisplay_fun.pdf}
		}
		\only<2>{
		\includegraphics[height = 0.24 \textheight]{figs/motivateReg_unReg.pdf} 
	%	\hspace{0.25cm}
		\includegraphics[height = 0.24 \textheight]{figs/motivateReg_reg.pdf} 
		\hfill
		\includegraphics[height = 0.24 \textheight]{figs/motivateData_hand.pdf} 
	%	\hspace{0.25cm}
		\includegraphics[height = 0.24 \textheight]{figs/motivateData_handOut.pdf} 
		}
		\only<3>{
		\includegraphics[width = 0.24 \textwidth]{figs/motivateData_growth.pdf}
		\hfill
		\includegraphics[width = 0.24\textwidth]{figs/motivateFPCA_mu.pdf} 
		\includegraphics[width = 0.24 \textwidth]{figs/motivateFPCA_1.pdf} 
		\includegraphics[width = 0.24 \textwidth]{figs/motivateFPCA_2.pdf} 
		}
		\only<4>{
\[
		\begin{aligned}
		& \text{\textcolor{myBlue}{Scalar}-on-\textcolor{lmuGruen}{Function}:} &
		\color{myBlue}{y_i} & = \mu + \int {\color{lmuGruen}{x_i(s)}} \beta(s) \drm s + \eps \\
&		\text{\textcolor{lmuGruen}{Function}-on-\textcolor{myBlue}{Scalar}:} &
		{\color{lmuGruen}{y_i(t)}}  & = \mu(t) + {\color{myBlue}{x_i}} \beta(t) + \eps(t) \\
&		\text{\textcolor{lmuGruen}{Function}-on-\textcolor{lmuGruen}{Function}:} &
		{\color{lmuGruen}{y_i(t)}} & = \mu(t) + \int {\color{lmuGruen}{x_i(s)}} \beta(s,t) \drm s + \eps(t)
		\end{aligned}
		\]
		}
	\end{center}
	\end{overlayarea}	
	
	\vfill
	
	\hfill   \only<1-3,5->{\footnotesize \cite{RamsaySilverman:2005}}
}


\subsection{From high-dimensional to functional data}

\begin{frame}
\frametitle{\secname}
\framesubtitle{\subsecname}

\textbf{Standard setting in multivariate data analysis:}

\begin{center}
\begin{tikzpicture}[minimum size = 0.6cm, scale = 0.7, transform shape, node distance = 1cm and 0.5cm]
\node[fill = mygreen!36!green, circle, ] (a1) {};
\node[fill = myblue!42!blue, circle, right = of a1] (a2) {};
\node[fill = brown!74!black, circle, right = of a2] (a3) {};
\node[fill = none, circle, right = of a3] (a4) {$\ldots$};
\node[fill = myred!75!red, circle, right = of a4] (a5) {}; 
%
\begin{pgfonlayer}{bg}    % select the background layer
\node[fill = black!15!white, fit = (a1) (a5), transform shape = false, rounded corners = .15cm] (r1) {};
\end{pgfonlayer}
% 
\node[fill = mygreen!32!green, circle, below of = a1] (b1) {};
\node[fill = myblue!74!blue, circle, right = of b1] (b2) {};
\node[fill = brown!57!black, circle, right = of b2] (b3) {};
\node[fill = none, circle, right = of b3] (b4) {$\ldots$};
\node[fill = myred!60!red, circle, right = of b4] (b5) {}; 
%
\begin{pgfonlayer}{bg}    % select the background layer
\node[fill = black!15!white, fit = (b1) (b5), transform shape = false, rounded corners = .15cm] (r2) {};
\end{pgfonlayer}
% 
\node[fill = mygreen!58!green, circle, below of = b1] (c1) {};
\node[fill = myblue!23!blue, circle, right = of c1] (c2) {};
\node[fill = brown!82!black, circle, right = of c2] (c3) {};
\node[fill = none, circle, right = of c3] (c4) {$\ldots$};
\node[fill = myred!30!red, circle, right = of c4] (c5) {}; 
%
\begin{pgfonlayer}{bg}    % select the background layer
\node[fill = black!15!white, fit = (c1) (c5), transform shape = false, rounded corners = .15cm] (r3) {};
\end{pgfonlayer}
% 
\node[fill = mygreen!61!green, circle, below of = c1] (d1) {};
\node[fill = myblue!43!blue, circle, right = of d1] (d2) {};
\node[fill = brown!75!black, circle, right = of d2] (d3) {};
\node[fill = none, circle, right = of d3] (d4) {$\ldots$};
\node[fill = myred!50!red, circle, right = of d4] (d5) {}; 
%
\begin{pgfonlayer}{bg}    % select the background layer
\node[fill = black!15!white, fit = (d1) (d5), transform shape = false, rounded corners = .15cm] (r4) {};
\end{pgfonlayer}
% 
 % 
\draw [decorate, decoration = brace, ultra thick, gray] ([yshift = -2mm]r4.south east) -- ([yshift = -2mm]r4.south west) node[midway, below, yshift = -0.1cm] {p variables};
\draw [decorate, decoration = brace, ultra thick, gray] ([xshift = -2mm]r4.south west) -- ([xshift = -2mm]r1.north west)  node[midway, below, xshift = -0.1cm, rotate = - 90] {n observations};

\end{tikzpicture}
\end{center}

\pause

\begin{itemize}
\item Observations $x_i = (x_{i1} \usw x_{ip})$ for $i = 1 \usw n$
\pause
\item Model complexity increases with $p$ (\textit{Curse of Dimensionality})
\end{itemize}

\end{frame}


\begin{frame}
\frametitle{\secname}
\framesubtitle{\subsecname}

\textbf{Data with natural ordering:}

\begin{center}
\input{figs/orderData_1D.tex}
%
\hspace*{0.5cm}%\hfill
%
\input{figs/orderData_1D_plot.tex}
\end{center}

\vspace*{-0.25cm}
\pause

\begin{itemize}
	\item Longitudinal data
	\item Ordering along time domain (one-dimensional)
\end{itemize}


\pause


\textbf{Functional data:}

\begin{center}
\input{figs/orderData_1D.tex}
%
\hspace*{0.5cm}
%
\input{figs/funData_1D.tex}
\end{center}

\pause

\begin{itemize}
\item Basic idea: Model discretely observed data by functions on  domain $\calT$
\end{itemize}

\end{frame}


\begin{frame}
\frametitle{\secname}
\framesubtitle{\subsecname}

\textbf{Functional data:}

\begin{center}
\input{figs/funData_1D_data.tex}
%
\hspace*{0.5cm}
%
\uncover<3->{\input{figs/funData_2D.tex}}
\end{center}

\vspace*{0.25cm}

\begin{itemize}
\item Observations $x_i(t), t \in \calT$ for $i = 1 \usw n$
\pause
\item Number of observable values $x_i(t_1) \usw x_i(t_p)$
\begin{itemize}
\item in theory: $p \to \infty$
\item in practice: $p < \infty$
\end{itemize}
\pause

\item Domain $\calT$
\begin{itemize}

	\item Realizations $x_1 \usw x_n$ of $X$ are curves ($d=1$), images ($d = 2$), 3D arrays ($d = 3$), etc.
	\end{itemize} 
\end{itemize}


\end{frame}




