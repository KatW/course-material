---
title: "Challenges lecture 01-04 Graphics"
author: "Heidi Seibold"
date: "MMS summer school"
output: ioslides_presentation
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
gapminder <- read.csv("https://swcarpentry.github.io/r-novice-gapminder/data/gapminder_data.csv", header=TRUE)
library("ggplot2")
```


## Challenge 1

- Within R Studio, click *File* &rarr; *New File* &rarr; *R Markdown* and in the dialog box choose *Presentation* and then *HTML (ioslides)*. You can enter a title (e.g. *my first slides*) and your name.
- Save the document that opens (e.g. *my_first_slides.Rmd*).
- Click the *knit* button. 
- Change a few things in the document and observe what changes in the slides (by looking at the knitted document):
    + Remove "`, echo = TRUE`" in the second R chunk
    + Add "`, echo = TRUE`" in the third R chunk
    + Add a fourth bullet point in the **Slide with Bullets**
    + Remove the stars `**` (`**Knit**` &rarr; `Knit`)
    

## Challenge 2
- Click the "File" menu button, then "New Project".
- Click "Version Control".
- Click "Git".
- Copy-Paste the Repository URL with the course material (https://gitlab.com/MMS-Summer-School-2018/course-material) 
and decide in which directory the project should be stored. Click "create project".
- Open the file `lecture-01-r-intro/lecture-01-03-data-wrangling-challenges.Rmd`
- Press knit. Have you seen the document before?
