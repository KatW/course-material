\documentclass[
%handout
]{beamer}

\newcommand{\chapternr}{8}

\newcommand{\semester}{Oberwolfach}

%\usepackage{../beamerLMU/beamerthemelmu}
% \documentclass[display]{FLslides}


%\input{../titelfolie}
\input{../header/defs}

% Coding and Languages
\usepackage[utf8]{inputenc} %Codierung für Linux
\usepackage[ngerman, english]{babel}

\beamertemplatenavigationsymbolsempty

%\usetikzlibrary{external}
%\tikzexternalize[prefix=figs/]


\setbeamertemplate{caption}{\insertcaption} 
\setbeamertemplate{caption label separator}{}
\setbeamertemplate{blocks}[rounded][shadow=true]

\setbeamertemplate{section in toc}[sections numbered]
\setbeamertemplate{subsection in toc}[subsections numbered]
\AtBeginSection[]
{
\addtocounter{framenumber}{-1}
\begin{frame}[plain]
\frametitle{Lecture \chapternr{} -- Overview}
\tableofcontents[currentsection,subsectionstyle=show/show/hide]
\end{frame}
}


\usepackage{arydshln}
\usepackage[round]{natbib}

\sloppy

\newcommand{\EEG}{\textnormal{\tiny \textsc{EEG}}}
\newcommand{\EMG}{\textnormal{\tiny \textsc{EMG}}}

\newcommand{\NIR}{\textnormal{\tiny \textsc{NIR}}}
\newcommand{\UVVIS}{\textnormal{\tiny \textsc{UV}}}
\newcommand{\ho}{\textnormal{\tiny \textsc{h2o}}}
\newcommand{\heat}{\textnormal{\tiny \textsc{heat}}}

\title{\textbf{MMS Summer School 2018}\\[0.5cm]Lecture \chapternr:\\Regression for Functional Data}
\author{Almond Stöcker}
\institute{LMU München}
\date{\semester}

\setbeamertemplate{footline}[text line]{%
  \parbox{\textwidth}{\vspace*{-8pt}MMS Summer School 2018 -- \semester \hfill Lecture \chapternr ~-- Slide \insertframenumber/\inserttotalframenumber}}

\setbeamercolor{alerted text}{fg=lmuGruen}

%\newcommand{\bi}{\begin{itemize}}
%\newcommand{\ei}{\end{itemize}}
%\newcommand{\ra}{\rightarrow}

\input{newCommands.tex} %$

\begin{document}

\addtocounter{framenumber}{-1}
\begin{frame}[plain]
	\titlepage
\end{frame}

% ~~~~~~~~~~~ ~~~~~~~~~~~~~~~ ~~~~~~~~~~~~~~ ~~~~~~~~~~~~~~

\section{Introduction}
\subsection{Functional regression problems}

\begin{frame}
\frametitle{\secname}
\framesubtitle{\subsecname}

Analyze, interpret and test effects of covariates on response observations involving functional data.
\begin{itemize}
	\item different generalizations from usual scalar model types, such as (generalized) linear regression, kernel regression,...
	\item overviews of functional regression:\newline \cite{morris2015}, \cite{greven2017}
\end{itemize}
\vspace{.7cm}
\centering
\begin{tikzpicture}[sibling distance=10em,
every node/.style = {shape=rectangle, rounded corners,
	draw, align=center,
	top color=white, bottom color=blue!20}]]
%% Draw system flow diagram
\begin{scope} [very thick, node distance=1cm,>=stealth', block/.style={rectangle,draw,fill=cyan!20}, comp/.style={circle,draw,fill=orange!40}] %xshift=-7.5cm,yshift=-5cm,
\node [block] (re)					{function-on-function};
%\node [comp]	 (cb)	[above=of re]			{B}  edge [->] (re);
%\node [comp]	 (ca1)	[above=of cb,xshift=-0.8cm]	{A1} edge [->] (cb);
%\node [comp]	 (ca2)	[right=of ca1]			{A2} edge [->] (cb);
\node [block] (s1)	[above=of re, xshift=-1.8cm]		{function-on-scalar} edge (re);
\node [block] (s2)	[right=of s1]		{scalar-on-function} edge (re);
\end{scope}
\end{tikzpicture}

\end{frame}

% ------------ ---------------- ----------------- ---------------

\subsection{Motivation: emotion components data}

\begin{frame}
\frametitle{\secname}
\framesubtitle{\subsecname}

	Data set from \citet{Gentsch.2014}, also used in \citet{rugamer2016}
	\vspace*{0.2cm}
	\begin{itemize}
		\item Main goal: Understand how emotions evolve
		\item Participants played a gambling game with real money outcome
		\item Emotions ``measured'' via EMG (muscle activity in the face)
		\item Influencing factor \emph{appraisals} measured via EEG (brain activity)
		\item Different game situation, a lot of trials
	\end{itemize}
\end{frame}

% ------------ ----------------- -------------------- ----------------------- -------------------

\frame{
	\frametitle{\secname}
	\framesubtitle{\subsecname}
	\action<+->{}
	\begin{figure}
		\includegraphics[width=\columnwidth]{figures/explData-1}
	\end{figure}
	\vspace*{-0.5cm}
	\hfill  {\footnotesize \citep{brockhaus2017}}\\
	\vspace*{0.2cm}
	\action<+->{ Function-on-function-regression } \action<+->{\ldots what for?}
	
	\begin{itemize}
		\item<+-> The value at a particular time-point is not really of interest
		\item<+-> Describe the course of each curve, more specifically their relationship $\rightarrow$ average course of function
	\end{itemize}
}

% -------------- -------------------- ---------------------- -------------------


%%%%%%%%%%%%%%%%% generic model

\section{Functional additive regression models}
\subsection{Generic model formulation}

\begin{frame}
	\frametitle{\secname}
	\framesubtitle{\subsecname}
	\begin{itemize}
		\item functional response $Y(t)$, $t \in \mathcal{T} = [T_1, T_2]$
		\item vector of covariates $\bm{x}$ containing functional covariates $x(s)$ and scalar covariates $z$
	\end{itemize}
	
	\begin{block}{Generic model}
		\begin{equation}
		\nonumber
		\mathbb{E}\left(Y(t)\mid \bm{x}\right) =  h(\bm{x}, t) = {\sum}_{j} \, \alert<.(3)>{h_j(\bm{x}, t)}
		\end{equation}
	\end{block}
	
	\begin{columns}
		\begin{column}[b]{0.45\textwidth}
			\bi
			\item $h(\bm{x}, t)$ linear predictor which is the sum of partial effects $h_j(\bm{x}, t)$
			\item[]
			\ei
		\end{column}
		\begin{column}[b]{0.55\textwidth}
			\bi
			\item each $h_j(\bm{x}, t)$ is a real valued function over $\mathcal{T}$ and can depend on one or several covariates
			\item[]
			\ei
		\end{column}
	\end{columns}
	
	\pause
	\bigskip
	$\ra$ Scalar response as degenerated case with $\mathcal{T}=[T_1,T_1]$
	
\end{frame}

\note{
	\bi
	\item I assume data with a \textbf{functional response} $Y$ of $t$, where $t$ is a real valued interval, use time $t$ throughout the talk as time is easy to imagine (functional variables are from the space of square integrable functions)
	
	\item the \textbf{transformation function} $\oper$ 'ksi' allows for flexible modeling, $\oper$ can be for example the mean, some quantile or expectile
	\item for GLM $\oper$ is the composition of the expectation and the link function $\oper=g \circ E$
	%\item transformation function is modeled using an adequate risk function
	
	\item on the right side you have an \textbf{additive predictor}. each effect can depend on several covariates.
	
	\item I want to emphasize the \textbf{modular structure} of the model: you have transformation function, and the additive predictor, any desired combinations of covariates effects and responses are possible!
	\ei
}

% --------------- ------------------------ ----------------------- -----------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% partial effects

\subsection{Partial effect $h_j(x, t)$}

\frame{
	\frametitle{\secname}
	\framesubtitle{\subsecname} 
	
	Effects of scalar covariates: 
	\bi
	\item<1-> smooth intercept $\beta_0(t)$  
	\item<1-> group-specific smooth intercepts $\beta_{0a}(t)$  
	%\item<1-> curve-specific smooth intercepts $\beta_{0i}(t)$  
	
	\bigskip
	\item<1-> smooth linear effect of scalar covariate $z\beta(t)$   
	\item<1-> smooth non-linear effect of scalar covariate $g(z,t)$     
	%\item<1-> smooth group-specific linear effect of a scalar covariate $z\beta_{1a}(t)$ 
	
	\bigskip
	\item<1-> interactions, e.g., $z_1 z_2 \beta(t)$ and $g(z_1, z_2, t)$ 
	\ei  
	
	
	\bigskip
	%\begin{flushright}{\small (see Scheipl et al., 2015; Brockhaus et al., 2015)}\end{flushright}
	\begin{flushright}{\small \citep{scheipl2015,brockhaus2015}}\end{flushright}
}
\note{
	Give some examples for effects: 
	(remember: the response is a function of $t$, thus all the effects are functions in $t$)
	\bi
	\item first there is a smooth intercept 
	\item the effect of a scalar covariate can be linear or non-linear 
	\ei 
}

% ----------------- ----------------------- ------------------------ -----------------------

\frame{
	\frametitle{\secname}
	\framesubtitle{\subsecname}
	\vspace*{0.3cm}
	
	Effects of functional covariates:
	\bi
	\item<+-> concurrent effect \only<5>{\color{blue}}$x(t) \beta(t)$
	\vspace*{0.1cm}
	\item<+-> linear effect of functional covariate  \only<6>{\color{lmuGruen}} $\int_{\mathcal{S}} x(s)\beta(s,t)\;ds$
	\item<+->[]	$\rightarrow$ for scalar response $\int_{\mathcal{S}} x(s)\beta(s)\;ds$
	\vspace*{0.2cm}
	\action<+->{
		\item constrained effect of functional covariate   \only<7>{\color{lmuGruen}} $\int_{l(t)}^{u(t)}\! x(s)\beta(s,t)\;ds$ \\
		%with integration limits $[l(t), u(t)]$
		%\textcolor{lmuGruen}{historical effect} 
		%$$\int_{T_1}^{t}\! x(s)\beta(s,t)\;ds$$
		%and concurrent effect $x(t)\beta(t)$
		\ei
	}
	\action<+->{
		\bi \item[]
		\renewcommand\arraystretch{1.4}
		\bigskip
		\begin{tabular}{l|rccc}
			%\hline
			effect & &  \only<6>{\color{lmuGruen}} linear &   \only<7>{\color{lmuGruen}} historical & 
			\only<7>{\color{lmuGruen}} lag 
			% & lead 
			% & concurrent 
			\\
			\hline
			$[l(t), u(t)]$ & & 
			\only<6>{\color{lmuGruen}} $[T_1, T_2]$ & 
			\only<7>{\color{lmuGruen}}  $[T_1, t]$ & %limits
			\only<7>{\color{lmuGruen}} $[t - \delta, t]$ %& 
			% $[T_1, t - \delta]$ & 
			% $[t, t]$ 
			\\
			& t
			& \includegraphics[width=0.16\textwidth, page=1]{lim_signal.pdf}
			& \includegraphics[width=0.16\textwidth, page=1]{lim_hist.pdf}  
			& \includegraphics[width=0.16\textwidth, page=1]{lim_lag.pdf}
			% & \includegraphics[width=0.16\textwidth, page=1]{lim_lead.pdf}
			% & \includegraphics[width=0.16\textwidth, page=1]{lim_conc.pdf} 
			\\ 
			\multicolumn{1}{r}{}  & & & s & \\
			%\hline
		\end{tabular}
		\ei
		
		%\bigskip
		%\begin{flushright}{\small (see Scheipl et al., 2015; Brockhaus et al., 2015/2016a)}\end{flushright}
		
		\begin{flushright}{\small \citep{scheipl2015,brockhaus2015hist,brockhaus2016}}\end{flushright}
	}
}

% ----------------------- --------------------- ------------------------ -----------------------

\subsection{Interactions of functional and scalar covariates}

\frame{
	\frametitle{\secname}
	\framesubtitle{\subsecname} 
	
	%\bigskip
	%Interactions between scalar and functional covariates
	\action<+->{
		\bi 
		\item linear interaction of scalar and functional covariate $$z \int_{l(t)}^{u(t)} x(s) \beta(s,t) ds$$
		\item group-specific functional effects $$I(z=a) \cdot \int_{l(t)}^{u(t)} x(s) \beta_a(s,t) ds $$\\ with indicator function $I(\cdot)$
		\ei 
	}
	
	\bigskip
	\bigskip
	\bi
	\item[$\rightarrow$]<+-> For all the listed effects ensure identifiability by suitable constraints
	\ei
}

% --------------- ------------------------- ------------------- -------------------

\frame{
	\frametitle{Specification of partial effects}
	{\small The generic model: 
		$\mathbb{E}(Y(t)| \bm{x}) =  h(\bm{x},t) = {\sum}_{j} \textcolor{lmuGruen}{h_j(\bm{x},t)}$
	}
	
	\bigskip
	\begin{block}{Row tensor product basis}
		\begin{equation}
		\nonumber
		h_j(\bm{x},t) = \left\{ \mb_{j}(\bm{x},t)^\top \odot \mb_Y(t)^\top \right\} \mtheta_j
		\end{equation}
	\end{block}
	%
	\begin{itemize} 
		\item $\mb_{j}$ / $\mb_Y$ vector of $\kappa_j$/$\kappa_Y$ basis functions in covariates / over $\mathcal{T}$ 
		\item $\odot$ row-wise tensor product ('Kronecker product on rows')
		%\item $\otimes$ Kronecker product 
		\item $\mtheta_j$ coefficient vector 
	\end{itemize}
	%\pause
	\begin{itemize}
		\item Ridge-type penalty 
		with penalty term $\mtheta_j^\top \mathbf{P}_{j Y} \mtheta_j$ 
		for regularization in both directions 
		%\\ 
		%penalty e.g.~$\mathbf{P}_{j Y} =  \lambda_{j} (\mathbf{P}_{j} \otimes \mathbf{I}_{K_Y}) +  \lambda_Y ( \mathbf{I}_{K_{j}} \otimes \mathbf{P}_{Y})$
		%\begin{itemize}
		%\item with $\mathbf{P}_{j}$, $\mathbf{P}_Y$ penalty matrices for $\mb_{j}$/$\mb_Y$
		%\item $\lambda_{j}$, $\lambda_Y \geq 0$ smoothing parameters 
		%\end{itemize}
		
		\pause 
		\bigskip
		\item if possible, representation as \textcolor{lmuGruen}{generalized linear array model} \\ \citep{currie2006}. 
	\end{itemize}
	
	%%%%% evtl eine neue Folie machen fuer array Modelle
	%\bi 
	%\item if possible use array framework of Currie et al., (2006), so-called functional linear array model (Brockhaus et al., 2015) $\ra$ more efficient computation  
	%\ei
} 
\note{
	now let's go into detail for the additive predictor
	\bi
	\item each effect is represented as tensor product basis of \textbf{two marginal bases}
	\item one for the covariates, and another over time, coefficient vector
	%\item possible to represent as \textbf{Kronecker} product only if the response is observed over a common grid and the effect can be split into two marginal bases where the basis in the covariates does not depend on $t$!
	\item for \textbf{regularization}: Ridge-type penalty and suitable penalty matrix that penalizes an effect in the direction of the covariates and of time; \\
	the penalty induces: similar effects for similar values of the corresponding covariates and similar effects for close time-points of the response; smoothness assumptions 
	\item if possible we represent the model as generalized linear array model, which saves time and memory when computing the model 
	\ei 
}

% ++++++++++++++++++ +++++++++++++++++++++++ ++++++++++++++++++++++ ++++++++++++++++++++++

\subsection{Estimation approaches}

\begin{frame}{Estimation}

\action<+->{How do we estimate such models?}

Estimation via penalized log-likelihood

\end{frame}

\begin{frame}{Estimation}

\vspace*{0.2cm}

Estimation via Gradient Boosting

\bi
\item<+-> As per usual: Write down the (penalized) log-likelihood and maximize it \citep[see, e.g.,][]{scheipl2016gfamm}
\item<+-> Problem: Computational feasibility\\
\vspace*{0.2cm}
\action<+->{$\ra$ For example, consider a model with 
	\bi
	\vspace*{0.1cm}
	\item<+-> a factor-specific historical effect  $\int_{0}^{t}\! x(s)\beta_a(s,t)\;ds$
	\item<+-> factor with $\kappa_z = 10$ levels
	\item<+-> $\kappa_s = \kappa_t = 20$ B-spline bases for $\beta_a(s,t)$ smoothness in $s$ and $t$
	\ei
}
\vspace*{0.1cm}
\action<+->{$\Rightarrow \text{ncol}(\bm{X}) = \kappa_z \cdot \kappa_s \cdot \kappa_t$}\action<+->{$= 10 \cdot 20 \cdot 20 = 4000$}
\item<+-> So how can we handle and fit multiple of such partial effects at the same time?
\ei

\action<+->{$\ra$ component-wise boosting}

\end{frame}

% +++++++++++++++++ +++++++++++++++++++ +++++++++++++++++ +++++++++++++++++++++++


\subsection{Other transformations of the conditional response distribution}

\frame{
	\frametitle{Remember the generic model}
	\begin{itemize}
		\item functional response $Y(t)$, $t \in \mathcal{T} \subset \mathbb{R}$
		\item vector of covariates $\bm{x}$ containing functional covariates $x(s)$ and scalar covariates $z$
	\end{itemize}
	
	\begin{block}{Generic model}
		\begin{equation}
		\nonumber
		\only<1>{\textcolor{red}{\mathbb{E}}}\only<2-3>{\textcolor{red}{\oper}}\left(Y(t)\mid \bm{x}\right) =  h(\bm{x},t) = {\sum}_{j} \, h_j(\bm{x},t)
		\end{equation}
	\end{block}
	
	\begin{columns}
		\begin{column}[b]{0.45\textwidth}
			\bi
			\item $h(\bm{x},t)$ linear predictor which is the sum of partial effects $h_j(\bm{x},t)$
			\item[]
			\ei
		\end{column}
		\begin{column}[b]{0.55\textwidth}
			\bi
			\item each $h_j(\bm{x},t)$ is a real valued function over $\mathcal{T}$ and can depend on or several covariates
			\item[]
			\ei
		\end{column}
	\end{columns}
	
	\action<3>{
		\bi
		\item $\oper$ is some transformation function, e.g., a quantile or $\mathbb{E}$ %or $\text{Var}$
		\ei
	}
}




\frame{
	\frametitle{Transformation and loss functions}
	\bgroup
	\def\arraystretch{1.2}%  1 is the default, change whatever you need
	\vspace*{0.2cm}
	\action<+->{The generic model: 
		$\textcolor{lmuGruen}{\oper}(Y(t) | \bm{x}) =  h(\bm{x},t) = {\sum}_{j} h_j(\bm{x},t)$}
	\action<+->{
		\begin{table}[h]
			\begin{tabular}{l|l|l}
				%\hline 
				model & $\oper$ &  {loss function  $\rho$} \\
				\hline 
				LM       & $\mathbb{E}$ & $L_2$-loss  \\[0.7em] %=neg.~log-likelihood of normal dist. 
				GLM      & $g \circ \mathbb{E}$ & negative log-likelihood  \\[0.7em]
				median regression   & $Q_{0.5}$ & $L_1$-loss \\ [0.7em] 
				quantile regression & $Q_\tau$ & check function  \\ %[0.7em] 
				%\hline  
			\end{tabular}
		\end{table}
		\egroup
	}
	%\pause
	\action<+->{
		\bi
		\item<+->[$\ra$] GAMLSS \citep{rigby2005} also possible
		\item<+->[$\ra$] Loss function for trajectories $\hat{=}$ integrated loss over domain of response: $$\ell(Y,h(\bm{x})) = \int_\mathcal{T} \underbrace{\rho(Y(t),h(\bm{x},t))}_{\text{pointwise loss for $t$}} dt$$  
		\ei
	}
	%\vspace*{0.2cm} 
	%\begin{flushright}{\small (Brockhaus et al., 2015)}\end{flushright}
}
\note{
	\bi
	\item generally one has to choose the loss such that the transformation function is the optimum 
	\item to start with something simple: for the LM, modeling the expectation of a normal distribution one simply uses the L2 loss, which is equivalent to the negative log-likelihood of the normal distribution for the expectation 
	\item for a GLM one uses the negative log-likelihood depending on the expectation; the transformation function can contain a link function  
	%\item use an adequate loss function that represents the estimation problem
	\item the L1 loss yields median regression 
	\item more generally, using the check function yields quantile regression 
	
	\bigskip
	\item now we need a loss that is defined for \textbf{trajectories} and gives a single value for each trajectory. We get that by integrating the loss over the domain of the response
	\ei 
}

% +++++++++++++++ +++++++++++++++ +++++++++++++++++++++++ ++++++++++++++++++++++


\subsection{Implementation in \texttt{FDboost}}

\begin{frame}{Implementation}
Implemented in 
\bi 
\item \textsf{R} package \textcolor{lmuGruen}{\texttt{FDboost}} \citep{FDboost_gen}
\item based on \textsf{R} package \texttt{mboost} \citep{mboost_gen}
\item Extension to GAMLSS based on \texttt{gamboostLSS} \citep{gamboostLSS2015}
% \item efficient implementation based on generalized linear array models (Currie et al., 2006)
\ei

\bigskip
hosted on \url{https://github.com/boost-R}

\end{frame}

\begin{frame}{Implementation in \texttt{FDboost} (I)}

\action<+->{Goal: Make use of the modular implementation of \texttt{mboost}}

\bi
\item<+-> Scalar-on-function regression:\\
the loss and empirical risk is just as for 'scalar-on-scalar regression'
\bi
\vspace*{0.1cm}
\item<+-> We just have to implement new base-learners and 
\vspace*{0.1cm}
\item<+-> allow for different data input (e.g. matrices for curve observations)
\ei
\vspace*{0.1cm}
\item<+-> Function-on-function regression:\\
\bi
\vspace*{0.1cm}
\item<+->  Implement base-learner, which also vary in the direction of $t$ 
\vspace*{0.1cm}
\item<+-> The loss function is now an integral 
\bi
\item<+->[$\rightarrow$] Numerical integration scheme to approximate expected loss 
\ei
\ei
\ei

\end{frame}

\begin{frame}{Implementation in \texttt{FDboost} (II)}

\bi
\item Main fitting function: \\
\vspace*{0.1cm}
\quad \texttt{FDboost(formula, timeformula, data, ...)}
\vspace*{0.2cm}
%\begin{Schunk}
%\begin{Sinput}
%\begin{verbatim}
%FDboost(formula, timeformula, id = NULL, 
%                  numInt = 'equal', data, offset = NULL, ...)
%\end{verbatim}
%\end{Sinput}
%\end{Schunk}

\item\texttt{timeformula}
\bi
\item[]\texttt{= NULL} for scalar-on-function regression,  
\item[]\alert<.(2)>{\texttt{= $\sim$ bbs(t)}} for function-on-function regression
\ei
\vspace*{0.2cm}
\item Some of the base-learners for functional data:\\
\vspace*{0.2cm}
\renewcommand\arraystretch{1.6}
\begin{small}
\begin{tabular}{ll}
$z \beta(t)$ & \,\,\texttt{bolsc(z)} \only<2>{\textcolor{lmuGruen}{\texttt{\%O\% bbs(t)}}} \\
$f(z,t)$ &  \,\,\texttt{bbsc(z)} \only<2>{\textcolor{lmuGruen}{\texttt{\%O\% bbs(t)}}}\\
$z_1 z_2 \beta(t)$ & \,\,\texttt{bols(z1) \%Xc\% bols(z2)} \only<2>{\textcolor{lmuGruen}{\texttt{\%O\% bbs(t)}}}\\
$\int_\mathcal{S} x(s) \beta(s,t) ds$ & \,\,\texttt{bsignal(x, s = s)} \only<2>{\textcolor{lmuGruen}{\texttt{\%O\% bbs(t)}}} \\
$x(t) \beta(t)$ & \,\,\texttt{bconcurrent(x, s = s, time = t)}\\
$\int_{l(t)}^{u(t)} x(s) \beta(s,t)ds$ & \,\,\texttt{bhist(x, s = s, time = t, limits = ...)}\\
%$z \int_{T_1}^{t} x(s) \beta(s,t)ds$ & \,\,\texttt{bolsc(z) \% X \% bhistx(x, s = s, time = t, limits = , ...)}
\end{tabular}
\end{small}

\ei

\end{frame}

% ------------ ------------------- ----------------- --------------------


\begin{frame}[fragile]{Example: \texttt{FDboost} call}

\begin{verbatim}
FDboost(EMG ~ 1 + 
brandomc(id, df = 5) +
bhist(EEG, df = 20),
timeformula = ~ bbs(t, df = 4), 
control = boost_control(mstop = 5000, 
trace = TRUE), 
data = data)
\end{verbatim}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Case studies}

%\addtocounter{framenumber}{-1}
%\frame[plain]
%{
%\begin{center}
%\textcolor{lmuGruen}{\LARGE {\bfseries Case studies}}
%\end{center}
%}


\subsection{Functional response}

\addtocounter{framenumber}{-1}
\frame[plain]
{
\begin{center}
	\textcolor{lmuGruen}{\LARGE {\bfseries Case studies}}\\
	\textcolor{lmuGruen}{\large {\bfseries Functional response}}
\end{center}
}


\frame{
\frametitle{Emotion components data}
Goal:  Try to explain 
\begin{itemize}
	\item facial expressions (measured with EMG) 
	\item by brain activity (measured with EEG) 
\end{itemize}
$\rightarrow$ Function-on-function-regression 
\begin{figure}
	\includegraphics[width=\columnwidth]{figures/explData-1}
\end{figure}
\vspace*{-0.3cm}
\hfill  \citep{brockhaus2017}
}


\begin{frame}
\frametitle{Emotion components data}
Model equation:
\begin{overlayarea}{\textwidth}{1.5cm}
	\only<1>{$$y_\EMG(t) = \beta_0(t) + x_\EEG(t) \beta_1(t) + \eps(t)$$}
	%\only<2-4>{$$y_\EMG(t) = \beta_0(t) + \textcolor{lmuGruen}{\int}_{\,}^{\,} x_\EEG(\textcolor{lmuGruen}{s}) \beta_1(\textcolor{lmuGruen}{s}) \textcolor{lmuGruen}{\drm s} + \eps(t)$$}
	\only<2>{$$y_\EMG(t) = \beta_0(t) +  \textcolor{red}{\int}_{\,}^{\,} x_\EEG(s) \beta_1(s, \textcolor{red}{t}) \drm s + \eps(t)$$}
	\only<3>{$$y_\EMG(t) = \beta_0(t) + \int_{\textcolor{blue}{0}}^{\textcolor{blue}{t-\delta}} x_\EEG(s) \beta_1(s,t) \drm s + \eps(t)$$}
\end{overlayarea}
\begin{itemize}
	\item<+-> One-to-one relation between EEG and EMG $\rightarrow$ Concurrent effect
	%\item<+-> Incorporate all time points $\rightarrow$ \textcolor{lmuGruen}{Constant functional effect}
	%\begin{itemize}
	%\item<+-> The effect of EEG does not change over time $t$
	%\item<+-> Every time point of EEG can have an influence on EMG
	%\end{itemize}
	\item<+-> Response time specific effect $\rightarrow$ \textcolor{red}{Linear functional effect}
	\item<+-> EMG can only be influenced by EEG activities in the past\\ $\rightarrow$ \textcolor{blue}{Historical effect}
\end{itemize}
\end{frame}

\begin{frame}{Results for more complex model}
\includegraphics[width=0.85\textwidth]{figures/resultPlots1a-1}
\end{frame}


\subsection{Scalar response and functional covariates}

\addtocounter{framenumber}{-1}
\frame[plain]
{
\begin{center}
\textcolor{lmuGruen}{\LARGE {\bfseries Case studies}}\\
\textcolor{lmuGruen}{\large {\bfseries Scalar response and functional covariates}}
\end{center}
}

\frame{
\frametitle{Spectral data of fossil fuels}
\textbf{Goal: predict heat value} $y$ using the spectral measurements of NIR and UV spectra % $x_\NIR (s_\NIR)$ and $x_\UVVIS (s_\UVVIS)$  
%$x_i(s)$\\ 
% Scalar-on-function-regression: $\color{black}{y_i} = \mu + \int {\color{black}{x_i(s)}} \beta(s) \drm s + \eps_i$ 
%Spectral data of fossil fuels to predict heat value 
%\begin{columns}
%\begin{column}[c]{0.55\textwidth}
%    \begin{figure}
%    \includegraphics[width=\columnwidth, page=4]{figures/NIR_UVVIS}
%    \end{figure}
%\end{column}
%\begin{column}{0.45\textwidth}
%    \begin{figure}
%    \includegraphics[width=\columnwidth, page=1]{figures/NIR_UVVIS}
%    \end{figure}
%\end{column}\end{columns}

\includegraphics[width = 0.99\textwidth]{figures/fuel01-1}

\bigskip
\begin{flushright} \citep{brockhaus2015} \end{flushright}
}


\frame{
\frametitle{Data of fossil fuel: model}
Model equation:
\begin{align*}
y  &= \beta_0 + f(z_\ho) + \int_{\mathcal{S}_\NIR} x_\NIR (s_\NIR)  \beta_\NIR(s_\NIR) \,ds_\NIR + \\ 
&\ \int_{\mathcal{S}_\UVVIS} x_\UVVIS (s_\UVVIS)  \beta_\UVVIS(s_\UVVIS) \,ds_\UVVIS + \varepsilon,
\end{align*}
\begin{itemize}
\item heat value $y$
\item non-linear effect of water content (H2O)
\item linear functional effect of NIR and UV spectrum
\end{itemize}
}


\frame{
\frametitle{Data of fossil fuel: results}

Estimated effects with stopping iteration chosen by 10 fold bootstrap 

\hspace*{-1.4cm}		\includegraphics[width = 1.25\textwidth]{figures/fuel_results-1.pdf} 
}

\frame{
\frametitle{Data of fossil fuel: results}

\begin{itemize}
\item estimated effects on 100 bootstrap samples (gray lines) 
\item point-wise median (black lines)
\item point-wise 5 and 95\% quantiles (dashed red lines)
\end{itemize}

\hspace*{-1.4cm}	\includegraphics[width = 1.25\textwidth]{figures/sof_boot-1.pdf} 
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\section{Bibliography}

\begin{frame}[allowframebreaks]
\frametitle{References}
{
	\footnotesize
	\bibliographystyle{plainnat}
	\bibliography{literatur_entries}
	
}

\end{frame}

\end{document}

