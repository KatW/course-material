---
title: "Training session 01 R intro"
author: "Heidi Seibold"
date: "MMS summer school"
output: ioslides_presentation
---


## Let's work on your reproducibility skills!

### Git:
- Create a repository for your group inside the MMS summer school group on GitLab.
- Store your scripts etc. in this repository.
- If time: write a README.md file that introduces your project.

### RMarkdown
- Create slides about your group project using RMarkdown. You can use these on 
friday for presenting.
- Store the Rmd-file in the repository. 


*Feel free to distribute the tasks among the group members. If there is time, 
explain what you did and what the challenges were.*
