## Other material

Etherpad: https://public.etherpad-mozilla.org/p/mms


## Contribution guide

Please name all folders and files according to the following conventions:

- all lower case
- use numbers to create order
- distinguish between lecture and training session
- put files in the corresponding folders

| Type             | Naming convention                          | Example                          |
|------------------|--------------------------------------------|----------------------------------|
| Lecture folder   | `lecture-<id>-<name>`                      | `lecture-1-r-intro`              |
| Practical folder | `training-session-<id>-<name>`             | `training-session-1-r`           |
| Lecture file     | `lecture-<id>-<name>.<filetype>`           | `lecture-01-01-rstudio-intro.Rmd`|
| Practical file   | `training-session-<id>-<name>.<filetype>`  | `training-session-01-r-intro.R`  |

Furthermore:

- Only add figures and pdfs to version control that are **not** created using a
script/Rmd file.
- Save the figures needed in folder `fig`.



