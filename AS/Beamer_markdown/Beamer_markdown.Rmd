---
title: "Nice title goes here"
output: beamer_presentation
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
```

## R Markdown

This is an R Markdown presentation. Markdown is a simple formatting syntax for authoring HTML, PDF, and MS Word documents. For more details on using R Markdown see <http://rmarkdown.rstudio.com>.

When you click the **Knit** button a document will be generated that includes both content as well as the output of any embedded R code chunks within the document.

## Slide with Bullets

- Bullet 1
- Bullet 2
- Bullet 3

## Slide with R Output

```{r cars, echo = TRUE}
summary(cars)
```

## Slide with Plot
Preliminary analysis of the data
```{r pressure}
library(RColorBrewer)
#generate colour palette
coul <- brewer.pal(9, "BuPu") 
coul <- colorRampPalette(coul)(288)

#produce layout
layout(rbind(c(1, 1), c(2, 3)))
pie(rep(1, length(coul)), col = coul , labels = '', clockwise = TRUE, density = 30, lty = 1) 
plot(1:10)
plot(1:10)

```

