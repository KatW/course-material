
Render all documents needed for the course by

```
git clone git@gitlab.com:MMS-Summer-School-2018/course-material.git
cd course-material/lecture-01-r-intro/
make all
```