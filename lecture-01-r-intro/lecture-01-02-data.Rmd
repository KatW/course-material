---
title: "Working with data"
output: pdf_document
---
This material is taken and adapted from
http://swcarpentry.github.io/swc-releases/2016.06/r-novice-gapminder/04-data-structures-part1/
and licensed under CC BY 4.0.


teaching: 25

exercises: 20

total: 01:40 (at 11:10)

questions:

- "How can I read data in R?"
- "What are the basic data types in R?"
- "How do I represent categorical information in R?"

objectives:

- "To be aware of the different types of data."
- "To begin exploring data frames."
- "To be able to ask questions from R about the class, and structure of an object."

keypoints:

- "Use `read.csv` to read tabular data in R."
- "The basic data types in R are double, integer, complex, logical, and character."
- "Use factors to represent categories in R."

```{r setup, echo = FALSE}
## use as working directory the my_project/analysis to have the same wd as
## the students
knitr::opts_knit$set(root.dir = "my_project/analysis/")
```


```{r, include=FALSE}

## Save the data
cats_orig <- data.frame(coat = factor(c("calico", "black", "tabby")),
                   weight = c(2.1, 5.0, 3.2),
                   likes_string = c(1, 0, 1))
cats_bad <- data.frame(coat = factor(c("calico", "black", "tabby", "tabby")),
                   weight = factor(c(2.1, 5.0, 3.2, '2.3 or 2.4')),
                   likes_string = c(1, 0, 1, 1))
cats <- cats_orig
```




## Challenge 1 + 2


We can load this into R via the following:

```{r}
cats <- read.csv(file = "../data_raw/feline-data.csv")
cats
```

Easy right? Let's try it ourselves!

## Challenge 3


We can begin exploring our dataset right away, pulling out columns by specifying
them using the `$` operator:

```{r}
cats$weight
cats$coat
```

We can do other operations on the columns:

```{r}
## Say we discovered that the scale weighs two Kg light:
cats$weight + 2
paste("My cat is", cats$coat)
```

But what about

```{r}
cats$weight + cats$coat
```

Understanding what happened here is key to successfully analyzing data in R.

## Data Types

If you guessed that the last command will return an error because `2.1` plus
`"black"` is nonsense, you're right - and you already have some intuition for an
important concept in programming called *data types*. We can ask what type of
data something is:

```{r}
class(cats$weight)
class(cats$coat)
```


A user has added details of another cat. This information is in the file
`data_raw/feline-data_v2.csv`.

```{r, eval=FALSE}
coat,weight,likes_string
calico,2.1,1
black,5.0,0
tabby,3.2,1
tabby,2.3 or 2.4,1
```

Load the new cats data like before, and check what type of data we find in the
`weight` column:

```{r}
cats <- read.csv(file="../data_raw/feline-data_v2.csv")
cats
class(cats$weight)
```

Oh no, our weights aren't `numeric` anymore! If we try to do the same math
we did on them before, we run into trouble:

```{r}
cats$weight + 2
```

What happened? When R reads a csv file into one of these tables, it insists that
everything in a column be the same basic type; if it can't understand
*everything* in the column as a double, then *nobody* in the column gets to be a
double. The table that R loaded our cats data into is something called a
*data.frame*, and it is our first example of something called a *data
structure* - that is, a structure which R knows how to build out of the basic
data types.

We can see that it is a *data.frame* by calling the `class` function on it:

```{r}
class(cats)
```

In order to successfully use our data in R, we need to understand what the basic
data structures are, and how they behave. For now, let's work with the first
data set:

```{r, eval=FALSE}
cats <- read.csv(file="../data_raw/feline-data.csv")
```

```{r, include=FALSE}
cats <- cats_orig
```

To see which class each collumn in your data.frame is use `str()`:
```{r}
str(cats)
```

To get a summary of your data.frame use `summary()`:
```{r}
summary(cats)
```


## Factors

We see that `weight` and `likes_string` are numeric, so numbers, but `coat` is a
*factor*. Factors are typically used to represent categorical information. For
example, let's make a vector of strings labelling cat colorations for all the
cats:

```{r}
coats <- c('tabby', 'tortoiseshell', 'tortoiseshell', 'black', 'tabby')
coats
str(coats)
```

We can turn a vector into a factor like so:

```{r}
CATegories <- factor(coats)
class(CATegories)
str(CATegories)
```

Now R has noticed that there are three possible categories in our data - but it
also did something surprising; instead of printing out the strings we gave it,
we got a bunch of numbers instead. R has replaced our human-readable categories
with numbered indices under the hood, this is necessary as many statistical
calculations utilise such numerical representations for categorical data.


## Challenge 4 + 5


## Reading various data sources

To figure out how to read in a data set, just google:

**"R read xyz data"**

This will usually let you find a good solution.


## Challenge 6 (if time)
