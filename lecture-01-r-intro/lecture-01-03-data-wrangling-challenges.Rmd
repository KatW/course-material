---
title: "Challenges lecture 01-03 Data wrangling"
author: "Heidi Seibold"
date: "MMS summer school"
output: ioslides_presentation
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```


# Write all code from the challenges into the R script we created: `working_with_data.R`

## Challenge 1

Given the following code:

```{r}
x <- c(a = 5.4, b = 6.2, c = 7.1, d = 4.8, e = 7.5)
x
```

Come up with at least 2 different commands that will produce the following output:

```{r, echo=FALSE}
x[2:4]
```

After you find 3 different commands, compare notes with your neighbour. Did you have different strategies?

## Solution to challenge 1

```{r, eval=FALSE}
x[2:4]
x[-c(1,5)]
x[c("b", "c", "d")]
x[c(2,3,4)]
```

## Challenge 2

Given the following code:

```{r}
x <- c(a = 5.4, b = 6.2, c = 7.1, d = 4.8, e = 7.5)
x
```

Write a subsetting command to return the values in x that are greater than 5 and less than 7.

## Solution to challenge 2

```{r}
x_subset <- x[x < 7 & x > 5]
x_subset
```


## Challenge 3

Create a new `data.frame` called `gapminder_small` that only contains
countries Sweden, Norway, and Finland.

Hint 1: use `gapminder$___ %in% c(___)`.

Hint 2: Remeber that for easier readability you can do this in several steps
as we did for the complex selection example in vectors:
```{r, eval=FALSE}
A <- x > 7 
B <- names(x) %in% c("a", "c")

x[A | B]
x[A & B]
```


```{r, echo=FALSE}
gapminder <- read.csv("https://swcarpentry.github.io/r-novice-gapminder/data/gapminder_data.csv", header=TRUE)
```


## Solution to challenge 8

```{r}
sel_c <- gapminder$country %in% c("Sweden", "Norway", "Finland")
sel_y <- gapminder$year %in% c(2002, 2007)
gapminder_small <- gapminder[sel_c & sel_y, ]
```
