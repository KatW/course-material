---
title: "Creating Publication-Quality Graphics with ggplot2"
output: pdf_document
---

teaching: 15

exercises: 20

total: 02:40 (at 12:10)

questions:

- "How can I create publication-quality graphics in R?"
objectives:
- "To be able to use ggplot2 to generate publication quality graphics."
- "To apply geometry, aesthetic, and statisics layers to a ggplot plot."
- "To manipulate the aesthetics of a plot usng different colors, shapes, and lines."
- "To improve data visualization through transforming scales and paneling by group."
- "To save a plot created with ggplot to disk."

keypoints:

- "Use `ggplot2` to create plots."
- "Think about graphics in layers: aesthetics, geometry, statistics, scale transformation, and grouping."


```{r, include=FALSE}
# Silently load in the data so the rest of the lesson works
gapminder <- read.csv("https://swcarpentry.github.io/r-novice-gapminder/data/gapminder_data.csv", header=TRUE)
```

Plotting our data is one of the best ways to
quickly explore it and the various relationships
between variables.

There are three main plotting systems in R,
the [base plotting system][base], the [lattice][lattice]
package, and the [ggplot2][ggplot2] package.

[base]: http://www.statmethods.net/graphs/
[lattice]: http://www.statmethods.net/advgraphs/trellis.html
[ggplot2]: http://www.statmethods.net/advgraphs/ggplot2.html

Today we'll be learning about the ggplot2 package, because
it is the most effective for creating publication quality
graphics.

ggplot2 is built on the grammar of graphics, the idea that any plot can be
expressed from the same set of components: a **data** set, a
**coordinate system**, and a set of **geoms**--the visual representation of data
points.

The key to understanding ggplot2 is thinking about a figure in layers.
This idea may be familiar to you if you have used image editing programs like Photoshop, Illustrator, or
Inkscape. 

Let's start off with an example:

```{r scatter, message=FALSE}
library("ggplot2")
g07 <- gapminder[gapminder$year == 2007, ]

ggplot(g07, aes(x = continent, y = gdpPercap)) + 
  geom_point()
```

So the first thing we do is call the `ggplot` function. This function lets R
know that we're creating a new plot, and any of the arguments we give the
`ggplot` function are the *global* options for the plot: they apply to all
layers on the plot.

We've passed in two arguments to `ggplot`. First, we tell `ggplot` what data we
want to show on our figure, in this example the gapminder data we read in
earlier. For the second argument we passed in the `aes` function, which
tells `ggplot` how variables in the **data** map to *aesthetic* properties of
the figure, in this case the **x** and **y** locations. Here we told `ggplot` we
want to plot the "continent" column of the gapminder data frame on the x-axis, and
the "gdpPercap" column on the y-axis. Notice that we didn't need to explicitly
pass `aes` these columns (e.g. `x = gapminder[, "gdpPercap"]`), this is because
`ggplot` is smart enough to know to look in the **data** for that column!

By itself, the call to `ggplot` isn't enough to draw a figure:

```{r}
ggplot(g07, aes(x = continent, y = gdpPercap))
```

We need to tell `ggplot` how we want to visually represent the data, which we
do by adding a new **geom** layer. In our example, we used `geom_point`, which
tells `ggplot` we want to visually represent the relationship between **x** and
**y** as a scatterplot of points:

```{r scatter2}
ggplot(g07, aes(x = continent, y = gdpPercap)) + 
  geom_point()
```


## Challenge 1 and 2


## Layers

In the two challenges we have learned how to change the layer we want. 
Both the type of layer `point` versus `jitter` and the details of the 
layer (e.g. `width` in `geom_jitter()`).

Until now we always copied the code from before and added a new layer.
We can also do this a bit smarter:

```{r add_layer}
p <- ggplot(g07, aes(x = continent, y = gdpPercap))

p + geom_point()
p + geom_boxplot()
p + geom_violin()

library("ggbeeswarm")
p + geom_quasirandom()
```

and we can even combine several layers in one plot:

```{r mult_layers}
p + geom_boxplot() + geom_jitter(width = 0.2, height = 0)
```

The astetics that we set in `ggplot()` hold for all layers. Sometimes we
want an aestetic just for one layer. This is also possible by adding `aes()`
to the specific `geom`.
```{r mult_layers2}
p + geom_boxplot(aes(fill = continent)) + geom_jitter(width = 0.2, height = 0)
```

Tip: Setting an aesthetic to a value instead of a mapping

So far, we've seen how to use an aesthetic (such as **color**) as a *mapping* to a variable in the data. For example, when we use `geom_boxplot(aes(fill = continent))`, ggplot will give a different color to each continent. But what if we want to change the colour of all boxplots to blue? You may think that `geom_boxplot(aes(fill = "blue"))` should work, but it doesn't. Since we don't want to create a mapping to a specific variable, we simply move the color specification outside of the `aes()` function, like this: `geom_boxplot(fill = "blue")`.

```{r mult_layers3}
p + geom_boxplot(aes(fill = "blue")) + geom_jitter(width = 0.2, height = 0)
p + geom_boxplot(fill = "blue") + geom_jitter(width = 0.2, height = 0)
```

## Challenge 3

## Multi-panel figures

A very common tool needed in publication figures is showing the same plot of 
subsets of the data. In our figure before we looked at only the year 2007. 
What if we want to do the same for all years?

- Option 1: create the data subset for each year and make the same graph
- Option 2: use ggplot2-Power to do it

Our figure we created before
```{r}
ggplot(g07, aes(x = continent, y = gdpPercap)) + 
  geom_boxplot(aes(fill = continent)) 
```

our figure for all years
```{r}
ggplot(gapminder, aes(x = continent, y = gdpPercap)) + 
  geom_boxplot(aes(fill = continent)) 
```
split up for the years
```{r}
ggplot(gapminder, aes(x = continent, y = gdpPercap)) + 
  geom_boxplot(aes(fill = continent)) + 
  facet_wrap(~ year)
```

## Modifying text

To make this figure really ready for publication we need to make the 
text a bit prettier. This can be done with `labs()`.

```{r}
ggplot(gapminder, aes(x = continent, y = gdpPercap)) + 
  geom_boxplot(aes(fill = continent)) + 
  facet_wrap(~ year) + 
  labs(x = "Continent",        
    y = "GDP per capita",  
    title = "Figure 1",    
    color = "Continent")
```



## Exporting the plot

The `ggsave()` function allows you to export a plot created with ggplot. You can specify the dimension and resolution of your plot by adjusting the appropriate arguments (`width`, `height`) to create high quality graphics for publication. In order to save the plot from above, we first assign it to a variable `fig1`, then tell `ggsave` to save that plot in `pdf` format.

```{r save}
fig1 <- ggplot(gapminder, aes(x = continent, y = gdpPercap)) + 
  geom_boxplot(aes(fill = continent)) + 
  facet_wrap(~ year) + 
  labs(x = "Continent",        
    y = "GDP per capita",  
    title = "Figure 1",    
    color = "Continent")

ggsave(filename = "gdp.pdf", plot = fig1, width = 12, height = 10)
```


This is a taste of what you can do with `ggplot2`. RStudio provides a
really useful [cheat sheet][cheat] of the different layers available, and more
extensive documentation is available on the [ggplot2 website][ggplot-doc].
Finally, if you have no idea how to change something, a quick Google search will
usually send you to a relevant question and answer on Stack Overflow with reusable
code to modify!

[cheat]: http://www.rstudio.com/wp-content/uploads/2015/03/ggplot2-cheatsheet.pdf
[ggplot-doc]: http://docs.ggplot2.org/current/


## Challenge 4 (if enough time)
