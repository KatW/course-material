---
title: "Challenges lecture 01-02 Working with data"
author: "Heidi Seibold"
date: "MMS summer school"
output: ioslides_presentation
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```


# Write all code from the challenges into the R script we created: `working_with_data.R`

# To remeber later what you did, use comments.

## Cats
<div class="columns-2">
calico:
```{r, out.width = "54%", echo=FALSE}
library("knitr")
include_graphics("https://upload.wikimedia.org/wikipedia/commons/2/22/Calico_and_dilute_calico_cats.JPG")
```

black:
```{r, out.width = "50%", echo=FALSE}
include_graphics("https://upload.wikimedia.org/wikipedia/commons/9/9b/Black_pussy_-_panoramio.jpg")
```

tabby:
```{r, out.width = "45%", echo=FALSE}
include_graphics("https://upload.wikimedia.org/wikipedia/commons/4/4d/Cat_November_2010-1a.jpg")
```

tortoiseshell:
```{r, out.width = "45%", echo=FALSE}
include_graphics("https://upload.wikimedia.org/wikipedia/commons/thumb/1/1e/Kira%2C_a_tortoise_shell_cat.jpg/1026px-Kira%2C_a_tortoise_shell_cat.jpg")
```
</div>


## Challenge 1

Tip: to figure out what `something` is for you look at the `properties` of the
folder `my_project`.

- Check if your working directory is `something/my_project/analysis/`
```{r, eval=FALSE}
# getwd = get working directory
getwd() 
```
- If not, set your working directory to `something/my_project/analysis/` using
```{r, eval=FALSE}
# setwd = set working directory
setwd("something/my_project/analysis/") 
```



## Challenge 2

- Create the `cats` data:
```{r, eval=TRUE}
cats <- data.frame(coat = c("calico", "black", "tabby"), 
                   weight = c(2.1, 5.0,3.2), 
                   likes_string = c(1, 0, 1))
```
- Save the `cats` data to the `data_raw` folder and call it `feline-data.csv` using
```{r, eval=FALSE}
write.csv(x = cats, file = "../data_raw/feline-data.csv", 
          row.names = FALSE)
```


## Challenge 3
Load the data in `feline-data.csv` into R via the following:
```{r, eval=FALSE}
cats <- read.csv(file = "../data_raw/feline-data.csv")
cats
```


## Challenge 4

What happens when you say
```{r, eval = FALSE}
as.numeric(cats$coat)
```
Why is that?

## Solution to Challenge 4

```{r}
as.numeric(cats$coat)
```
```{r}
cats$coat
labels(cats$coat)
levels(cats$coat)
```


## Challenge 5

- Read the gapminder data set into R (see etherpad for link)
```{r, eval=FALSE}
gapminder <- read.csv(
  "https://swcarpentry.github.io/r-novice-gapminder/data/gapminder-gapminder_data.csv"
  )
```
- Look at the structure `str()` and summary `summary()` of the cats data.frame.


## Solution to Challenge 5 (1)
```{r}
gapminder <- read.csv(
  "https://swcarpentry.github.io/r-novice-gapminder/data/gapminder_data.csv"
  )

str(gapminder)
```


## Solution to Challenge 5 (2)
```{r}
summary(gapminder)
```

## Challenge 6 (if time)

Do you work with a specific type of data source? Google how to read in this 
type of data. 

If you have nothing to look for try looking at excel files.

If you have enough time: try reading your data into R.


## "Solution" to Challenge 6

- What types of files are you working with?
- Did everyone find good help online?
- Were you able to read the data into R?

If we have time left over at the end of the morning session we will 
take a look at some specific examples.
