#########################
# PACE for full dataset #
#########################

# functional PCA
Fit <- myPACE(X=age, Y=dat,npc=2)

# proportion of variance: fit model with many functional principal components
allFit <- myPACE(X=age, Y=dat, pve = 1-1e-5)
(allFit$evalues)/(sum(allFit$evalues) + allFit$sigma2)

# plot mean plus effects of first and second functional principal components
plot(age, Fit$mu, type='l', ylim=c(60, 200),
     xlab="Age", ylab="Height", main="Effect of PC 1")
points(age, Fit$mu+30*Fit$efunctions[,1], pch="+")
points(age, Fit$mu-30*Fit$efunctions[,1], pch="-")
plot(age, Fit$mu, type='l', ylim=c(60, 200),
     xlab="Age", ylab="Height", main="Effect of PC 2")
points(age, Fit$mu+30*Fit$efunctions[,2], pch="+")
points(age, Fit$mu-30*Fit$efunctions[,2], pch="-")

# plot scores
plot(Fit$scores[,1], Fit$scores[,2],
     pch=20, col=ifelse(sex=="f", "red","blue"),
     main="Functional Principal Component Scores",
     xlab="Score Component 1", ylab="Score Component 2")
legend("bottomright", legend=c("girls", "boys"), col=c("red","blue"), pch=20)