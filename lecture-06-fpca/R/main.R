################
# Main Program #
################

library(fda) # load library
source("utilities.R") # utility functions

pdf("../figs/FPCAplots.pdf", height = 5, width = 5)
par(mar = c(5,5,1,1) + 0.1)

set.seed(1234)

### data processing ###
data(growth)

age <- growth$age
height <- cbind(growth$hgtm, growth$hgtf)
sex <- c(rep("m", dim(growth$hgtm)[2]), rep("f", dim(growth$hgtf)[2]))
dat <- t(height) # for myPACE function
n <- dim(height)[1] # number of observations
N <- dim(height)[2] # number of individuals

# sparsify data artificially
height_sparse <- sparsify(dat, minObs=2, maxObs=6)

median(rowSums(!is.na(height_sparse))) # numbers of observation per age: median 
sum(!is.na(height_sparse))/(n*N) # relative number of observations

# draw random individuals for visualization
randind <- sample(N,4)


### visualization ###
# full dataset
matplot(age, height, type="l", lty=1, col=ifelse(sex == "f", "red","blue"),
        xlim=c(1,18), ylim=c(60, 200), xlab="Age", ylab="Height")
rug(age)
legend("topleft", legend=c("girls", "boys"), col=c(2,4), lty=1)

# sparsified dataset
matplot(age, height[,randind], type="l", lty=2,
        xlim=c(1,18), ylim=c(60, 200), xlab="Age", ylab="Height")
rug(age)
for(i in randind)
    points(age[!is.na(height_sparse[i,])], height_sparse[i,!is.na(height_sparse[i,])],
           pch=20, col=which(randind==i))
legend("topleft", legend=c("full growth curve","sparsified observations"),
        col=1, lty=c(3,NA), pch=c(NA, 20))


### Methods using fda-package ###
# "classical" fPCA
source("fPCA.R")

# smoothed fPCA
source("fPCA_smooth.R")

### Methods using myPACE function ###

# PACE for sparse dataset
source("PACE_sparse.R")

dev.off()
