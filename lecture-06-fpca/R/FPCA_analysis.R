### data analysis based on functional pca

library("fda") # for data and PCA calculation
library("depth") # for depth calculation

pdf("../figs/FPCA_analysis.pdf", height = 5, width = 6.5)
par(mar = c(5,5,1,1) + 0.1)

# load Canadian weather data and have a look at it
data("CanadianWeather")
CanadianWeather$dailyAv[1:10,1:5,"Temperature.C"]
head(day.5)

# choose basis (Fourier basis)
daybasis45 <- create.fourier.basis(c(0, 365), nbasis=45)

# create functional data object
temperature.fd <- Data2fd(y=CanadianWeather$dailyAv[,,"Temperature.C"], argvals=day.5, basis=daybasis45)

# plot the data
plot(temperature.fd, col = 1, lty = 1, xlab = "Day of Year", ylab = "Temperature [°C]")

# functional PCA
temperature.pca <- pca.fd(temperature.fd, nharm=2, centerfns=T)

### 1. depth calculation ###
allDepth <- apply(temperature.pca$scores, 1, depth::depth, x = temperature.pca$scores, method = "Tukey") * 35 # in our definition, depth must be integers!

medTemp = which(allDepth == max(allDepth)) # Tukey median
outTemp = which(allDepth == min(allDepth)) # potential outliers

# Plot scores
plot(temperature.pca$scores[,1], temperature.pca$scores[,2],
     xlab="Score PC 1", ylab="Score PC 2")
points(temperature.pca$scores[medTemp,1], temperature.pca$scores[medTemp,2],
       pch=20, col = "blue", cex = 1.5)
points(temperature.pca$scores[outTemp,1], temperature.pca$scores[outTemp,2],
       pch=20, col = "orange", cex = 1.5)

# Plot functions
plot(temperature.fd,  col = "grey", lty= 1, xlab = "Day of Year", ylab = "Temperature [°C]")
lines(temperature.fd[medTemp], col = "blue", lwd = 2, lty= 1)
lines(temperature.fd[outTemp], col = "orange", lwd = 1, lty = 1)


### 2. Clustering ###
set.seed(12345) # clustering is random, make reproducible

# K-Means clustering with 4 groups
clustTemp <- kmeans(temperature.pca$scores, centers = 4)

# plot clusters: scores and curves
plot(temperature.pca$scores[,1], temperature.pca$scores[,2],
     pch=20, cex = 1.5, col = clustTemp$cluster,
     xlab="Score PC 1", ylab="Score PC 2")
plot(temperature.fd,  col = clustTemp$cluster, lty= 1, xlab = "Day of Year", ylab = "Temperature [°C]")

# spatial distribution of the clusters
library(fields)
par(mar = c(0,0,0,0) + 0.1)
plot(-CanadianWeather$coordinates[, 2], CanadianWeather$coordinates[, 1],type = "n", axes = FALSE, frame.plot = TRUE,
     xlim = c(-140, -50), ylim = c(40, 75),
     xlab = "", ylab=  "")
world(add = TRUE, col = "grey")
text(-CanadianWeather$coordinates[, 2], CanadianWeather$coordinates[, 1], labels = CanadianWeather$place, col = clustTemp$cluster, cex = 0.75)
dev.off()